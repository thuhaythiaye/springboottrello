package com.thta.task.service;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.thta.task.model.BsUser;

@RunWith(SpringRunner.class)
@SpringBootTest
public class BsUserServiceTest {
	
	@Autowired
	private BsUserService bsUserService;
	
	@Test
	public void getAllBsUser() {
		List<BsUser> userList = bsUserService.getAllBsUser();
		assertThat(userList).isNotNull().isNotEmpty();
	}
	
	@Test
	public void createBsUser() {
		BsUser user = new BsUser();
		user.setUser_name("test");
		user.setUser_email("test@gmail.com");
		user.setUser_pwd("test");
		int createResult = bsUserService.createBsUser(user);
		assertThat(createResult).isNotEqualTo(0);
	}
	
	@Test
	public void updateBsUser() {
		BsUser user = new BsUser();
		user.setUser_email("test@gmail.com");
//		user.setUser_name("Test");
		user.setUser_pwd("123");
		int updateResult = bsUserService.updateBsUser(user);
		assertThat(updateResult).isNotEqualTo(0);		
	}
	
	@Test
	public void deleteBsUser() {
		BsUser user = new BsUser();		
		user.setUser_email("test@gmail.com");
		int createResult = bsUserService.deleteBsUser(user);
		assertThat(createResult).isNotEqualTo(0);
	}
	
	@Test
	public void checkUserEmail () {		
		boolean checkUserEmailResult = bsUserService.checkUserEmail("thi@gmail.com");
		assertThat(checkUserEmailResult).isNotEqualTo(false);
	}
	
	@Test
	public void getUserIdByUserEmail() {
		int getUserIdResult = bsUserService.getUserIdByUserEmail("thi@gmail.com");
		assertThat(getUserIdResult).isNotEqualTo(0);
	}
	
	@Test
	public void checkUserByUserId() {
		boolean checkUserResult = bsUserService.checkUserByUserId(5);
		assertThat(checkUserResult).isNotEqualTo(false);
	}

}