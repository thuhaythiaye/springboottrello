package com.thta.task;

import static org.hamcrest.CoreMatchers.is;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.thta.task.controller.UserController;
import com.thta.task.model.Customer;

@RunWith(SpringRunner.class)
// @SpringBootTest
@WebMvcTest(UserController.class)
public class BssTaskmanagementThtaApplicationTests {

	@Autowired
	private MockMvc mvc;

	@MockBean
	private UserController userController;

	private List<Customer> custList = new ArrayList<>();

	/*
	 * @Test public void contextLoads() { }
	 */

	@Before
	public void setup() throws Exception {
		this.mvc = MockMvcBuilders.standaloneSetup(this.userController).build();// Standalone context

		Customer cust = new Customer();
		cust.setCustName("thta");
		cust.setCountry("mm");
		custList.add(cust);
	}

	@Test
	public void getTaskAll() throws Exception {

		BDDMockito.given(userController.customerInformation()).willReturn(custList);
		mvc.perform(MockMvcRequestBuilders.get("/getcustInfo").contentType(MediaType.APPLICATION_JSON))
		.andDo(print())
		.andExpect(status().isOk())
		.andExpect(jsonPath("$[0].country", is("mm")));
	}

}