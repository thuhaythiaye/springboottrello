package com.thta.task.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.thta.task.model.Customer;
import com.thta.task.repository.BsUserRepository;
import com.thta.task.service.TodoDaoService;
import com.thta.task.service.UserControllerService;

/**
 * /login POST controller is provided by Spring Security
 */
@RestController
public class UserController {
	
	@Autowired
	UserControllerService userControllerService;

	@Autowired
	TodoDaoService todoDaoService;
	
	@Autowired
	BsUserRepository bsUserRepository;
	
	@RequestMapping("/welcome")
	public String welcome() {
		return userControllerService.showWelcomeMsg();
	}

	@RequestMapping("/getcustInfo")
	public List<Customer> customerInformation() {
		return todoDaoService.customerInformation();
	}
	
}