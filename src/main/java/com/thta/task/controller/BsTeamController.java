package com.thta.task.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.thta.task.model.BsMessage;
import com.thta.task.model.BsModal;
import com.thta.task.model.BsTeam;
import com.thta.task.service.BsTeamService;

@RestController
public class BsTeamController {

	@Autowired
	BsTeamService bsTeamService;

	@RequestMapping("/getAllTeamInfo")
	public List<BsTeam> getAllTeamInfo() {
		return bsTeamService.getAllTeamInfo();
	}

	@RequestMapping(value = "/getTeamsByUserId", method = RequestMethod.POST)
	public List<BsTeam> getTeamsByUserId(@RequestBody BsModal modal) {
		if (checkUserId(modal)) {
			return bsTeamService.getTeamsByUserId(modal.getUser_id());
		} else {
			return new ArrayList<BsTeam>();
		}
	}

	private boolean checkUserId(BsModal modal) {
		boolean result = false;
		if (modal != null) {
			if (modal.getUser_id() != 0) {
				result = true;
			}
		}
		return result;
	}

	@RequestMapping(value = "/createBsTeam", method = RequestMethod.POST)
	public BsMessage createBsTeam(@RequestBody BsModal modal) {
		BsMessage bsMsg = new BsMessage();
		bsMsg.setMsg_code("404");
		bsMsg.setMsg_title("Warning");
		bsMsg.setMsg_desc("Create Team Unsuccessfully.");

		if (checkCreateTeam(modal)) {
			if (bsTeamService.createBsTeam(modal) > 0) {
				bsMsg.setMsg_code("200");
				bsMsg.setMsg_title("Success");
				bsMsg.setMsg_desc("Create user successfully.");
			}
		}
		return bsMsg;
	}

	// To check team title that is required when create team.
	private boolean checkCreateTeam(BsModal modal) {
		boolean result = false;
		if (modal != null) {
			if (modal.getUser_id() != 0 && modal.getTeam_name() != null && !modal.getTeam_name().equals("")) {
				result = true;
			}
		}
		return result;
	}

	@RequestMapping(value = "/updateBsTeam", method = RequestMethod.POST)
	public BsMessage updateBsTeam(@RequestBody BsTeam team) {
		BsMessage bsMsg = new BsMessage();
		bsMsg.setMsg_code("404");
		bsMsg.setMsg_title("Warning");
		bsMsg.setMsg_desc("Create Team Unsuccessfully.");
		System.out.println(team);
		System.out.println(team.getTeam_id());

		if (checkUpdateTeam(team)) {
			if (bsTeamService.updateBsTeam(team) > 0) {
				bsMsg.setMsg_code("200");
				bsMsg.setMsg_title("Success");
				bsMsg.setMsg_desc("Create user successfully.");
			}
		}
		return bsMsg;
	}

	// To check team title that is required when create team.
	private boolean checkUpdateTeam(BsTeam team) {
		boolean result = false;
		if (team != null) {
			if (team.getTeam_id() != 0 && ((team.getTeam_name() != null && !team.getTeam_name().equals(""))
					|| (team.getTeam_desc() != null && !team.getTeam_desc().equals("")))) {
				result = true;
			}
		}
		return result;
	}

	@RequestMapping(value = "/deleteBsTeam", method = RequestMethod.POST)
	public BsMessage deleteBsTeam(@RequestBody BsTeam team) {
		BsMessage bsMsg = new BsMessage();
		bsMsg.setMsg_code("404");
		bsMsg.setMsg_title("Warning");
		bsMsg.setMsg_desc("Delete Board Unsuccessfully.");
		if (team.getTeam_id() != 0) {
			if (bsTeamService.deleteBsTeam(team) > 0) {
				bsMsg.setMsg_code("200");
				bsMsg.setMsg_title("Success");
				bsMsg.setMsg_desc("Delete Board Successfully.");
			}
		}
		return bsMsg;
	}

}