package com.thta.task.service;

import java.util.List;

import com.thta.task.model.Customer;

public interface TodoDaoService {
	
	List<Customer> customerInformation();

}