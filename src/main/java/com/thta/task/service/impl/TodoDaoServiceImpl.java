package com.thta.task.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.thta.task.dao.TodoDAO;
import com.thta.task.model.Customer;
import com.thta.task.service.TodoDaoService;

@Service
public class TodoDaoServiceImpl implements TodoDaoService{
	
	@Autowired
	public TodoDAO dao;
	
	@Override
	public List<Customer> customerInformation() {
		List<Customer> customers = dao.isData();
		return customers;
	}

}