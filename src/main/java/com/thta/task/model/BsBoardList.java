package com.thta.task.model;


public class BsBoardList {

	private int blist_id;
	private String blist_title;
	private int board_id;

	public int getBlist_id() {
		return blist_id;
	}

	public void setBlist_id(int blist_id) {
		this.blist_id = blist_id;
	}

	public String getBlist_title() {
		return blist_title;
	}

	public void setBlist_title(String blist_title) {
		this.blist_title = blist_title;
	}

	public int getBoard_id() {
		return board_id;
	}

	public void setBoard_id(int board_id) {
		this.board_id = board_id;
	}

}