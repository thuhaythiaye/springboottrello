package com.thta.task.model;

public class BsCard {

	private int card_id;
	private String card_title;
	private int blist_id;

	public int getCard_id() {
		return card_id;
	}

	public void setCard_id(int card_id) {
		this.card_id = card_id;
	}

	public String getCard_title() {
		return card_title;
	}

	public void setCard_title(String card_title) {
		this.card_title = card_title;
	}

	public int getBlist_id() {
		return blist_id;
	}

	public void setBlist_id(int blist_id) {
		this.blist_id = blist_id;
	}

}